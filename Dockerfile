# Fetch latest ubuntu image for the ImmunoProbs image build.
FROM ubuntu:latest

# Install some apt linux packages.
# edited to python3-pip ffrom python-pip
RUN apt-get update \
    && apt-get install -y \
    git \
    gcc-7 \
    g++-7 \
    build-essential \
    muscle \
    python2-dev \
    unzip \
    wget \
    curl

RUN whereis python

RUN curl https://bootstrap.pypa.io/pip/2.7/get-pip.py -o get-pip.py \
    && python2.7 get-pip.py \
    && pip2 --version 

# Download a version of IGoR, unpack it and compile.
RUN wget https://github.com/qmarcou/IGoR/releases/download/1.4.0/igor_1-4-0.zip \
    && unzip igor_1-4-0.zip && cd igor_1-4-0 \
    && ./configure CC=gcc-7 CXX=g++-7 \
    && make clean \
    && make \
    && make install \
    && cd .. \
    && rm -r igor_1-4-0 \
    && rm igor_1-4-0.zip 

# Install given version of ImmunoProbs.
RUN git clone https://bitbucket.org/jspagnuolo/immuno-probs.git immunoprobs \
    && cd immunoprobs \
    && python2.7 setup.py install

